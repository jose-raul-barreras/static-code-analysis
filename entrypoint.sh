#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER_ID=${LOCAL_USER_ID:-9001}

echo "Starting with UID : $USER_ID"
adduser -h /home/scanner -s /bin/bash -D -u $USER_ID scanner

#RUN addgroup -g $GID scanner
#RUN adduser -D -s /bin/bash -H -G scanner -g $UID -u $UID scanner
#RUN chown -R scanner /opt/src

su-exec user "$@"
