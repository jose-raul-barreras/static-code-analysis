#!/bin/sh
set -e

if [[ -n "$GID" ]]; then
    groupmod -o -g $GID scanner
fi

if [[ -n "$UID" ]]; then
    usermod -o -u $UID scanner
fi

# Re-set permission to the `scanner` user if current user is root
# This avoids permission denied if the data volume is mounted by root
if [ "$1" = 'scanner' -a "$(id -u)" = '0' ]; then
    chown -R scanner /opt/src
    exec su-exec scanner "$@"
fi

exec "$@"
